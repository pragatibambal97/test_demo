#include<iostream>
#include<string>
#include<stack>
#include<queue>
using namespace std;
namespace collection {
class Exception {
private:
	string message;
public:
	Exception(string messgae) throw () :
			message(messgae) {
	}
	string getMessage(void) const throw () {
		return this->message;
	}
};
class BSTree;
//Forward declaration
class Node {
private:
	Node *left;
	int data;
	Node *right;
public:
	Node(int data = 0) :
			left( NULL), data(data), right( NULL) {
	}
	friend class BSTree;
};
class BSTree {
private:
	Node *root;
public:
	BSTree(void) throw () :
			root( NULL) {
	}
	bool empty(void) const throw () {
		return this->root == NULL;
	}
	void addNode(int data) {
		Node *newNode = new Node(data);
		if (this->empty())
			this->root = newNode;
		else {
			Node *trav = this->root;
			while (true) {
				if (data < trav->data) {
					if (trav->left == NULL) {
						trav->left = newNode;
						break;
					}
					trav = trav->left;
				} else {
					if (trav->right == NULL) {
						trav->right = newNode;
						break;
					}
					trav = trav->right;
				}
			}
		}
	}
	Node* search(int data, Node *&parent) throw () {
		Node *trav = this->root;
		parent = NULL;
		while (trav != NULL) {
			if (data == trav->data)
				return trav;
			parent = trav;
			if (data < trav->data)
				trav = trav->left;
			else
				trav = trav->right;
		}
		parent = NULL;
		return NULL;
	}

	void nonRecursivePreorder(void) {
		stack<Node*> stk;
		stk.push(this->root);
		while (!stk.empty()) {
			Node *trav = stk.top();
			stk.pop();
			if (trav != NULL)
				cout << trav->data << "	";
			if (trav->right != NULL)
				stk.push(trav->right);
			if (trav->left != NULL)
				stk.push(trav->left);
		}
		cout << endl;
	}

	void inOrder(void) {
		this->inOrder(this->root);
		cout << endl;
	}
	void postOrder(void) {
		this->postOrder(this->root);
		cout << endl;
	}

	int height(void) {
		return BSTree::height(this->root);
	}
	~BSTree(void) {
		this->clear(this->root);
		this->root = NULL;
	}
private:
	void inOrder(Node *trav) {
		if (trav == NULL)
			return;
		this->inOrder(trav->left);
		cout << trav->data << "	";
		this->inOrder(trav->right);
	}
	void postOrder(Node *trav) {
		if (trav == NULL)
			return;
		this->postOrder(trav->left);
		this->postOrder(trav->right);
		cout << trav->data << "	";
	}

	static int height(Node *trav) {
		if (trav == NULL)
			return -1;
		int leftHeight = height(trav->left);
		int rightHeight = height(trav->right);
		if (leftHeight > rightHeight)
			return leftHeight + 1;
		return rightHeight + 1;
	}
	void clear(Node *trav) {
		if (trav == NULL)
			return;
		this->clear(trav->left);
		this->clear(trav->right);
		delete trav;
	}
};
}

int menu_list(void) {
	int choice;
	cout << "0.Exit" << endl;
	cout << "1.Search Node" << endl;
	cout << "2.Inorder Recursive" << endl;
	cout << "3.Postorder Recursive" << endl;
	cout << "4.Preorder Non-Recursive" << endl;
	cout << "5.Heightof Tree" << endl;
	cout << "6.Anchester or not" << endl;

	cout << "Enter choice	:	";
	cin >> choice;
	return choice;
}

int main(void) {
	int choice, n;

	using namespace collection;
	BSTree tree;

	cout << "Enter number of nodes to be added";
	cin >> n;
	//double arr[n];
	double *arr = new double[n];
	cout << "Enter node elements";
	for (int i = 0; i < n; i++) {
		cin >> arr[i];
		tree.addNode(arr[i]);
	}

	while ((choice = ::menu_list()) != 0) {
		try {
			switch (choice) {
			case 1:
				float node;
				cout << "Enter node to be searched";
				cin >> n;
				//tree.search(node);
				break;

			case 2:
				tree.inOrder();
				break;

			case 3:
				tree.postOrder();
				break;

			case 4:
				tree.nonRecursivePreorder();
				break;

			case 5:
				tree.height();
				break;

			case 6:

				break;

			}
		} catch (Exception &ex) {
			cout << ex.getMessage() << endl;
		} catch (bad_alloc &ex) {
			cout << ex.what() << endl;
		}

	}


	return 0;
}
